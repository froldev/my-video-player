<?php

namespace App\Tests;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Video;
use PHPUnit\Framework\TestCase;

class VideoTest extends TestCase
{
    public function testGetId()
    {
        $video = new Video();
        $this->assertNull($video->getId());
    }

    public function testGetTitle()
    {
        $video = new Video();
        $video->setTitle('Test Video');
        $this->assertEquals('test video', $video->getTitle());
    }

    public function testGetSlug()
    {
        $video = new Video();
        $video->setSlug('test-video');
        $this->assertEquals('test-video', $video->getSlug());
    }

    public function testGetUrl()
    {
        $video = new Video();
        $video->setUrl('https://example.com');
        $this->assertEquals('https://example.com', $video->getUrl());
    }

    public function testGetDescription()
    {
        $video = new Video();
        $video->setDescription('Lorem ipsum dolor sit amet');
        $this->assertEquals('Lorem ipsum dolor sit amet', $video->getDescription());
    }

    public function testIsHomepage()
    {
        $video = new Video();
        $video->setHomepage(true);
        $this->assertTrue($video->isHomepage());
    }

    public function testIsActive()
    {
        $video = new Video();
        $video->setActive(true);
        $this->assertTrue($video->getActive());
    }

    public function testGetCategory()
    {
        $video = new Video();
        $category = new Category();
        $video->setCategory($category);
        $this->assertEquals($category, $video->getCategory());
    }

    public function testGetAuthor()
    {
        $video = new Video();
        $author = new Author();
        $video->setAuthor($author);
        $this->assertEquals($author, $video->getAuthor());
    }
}