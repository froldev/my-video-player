<?php

namespace App\Tests;

use App\Entity\Author;
use App\Entity\Video;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetId()
    {
        $author = new Author();
        $this->assertNull($author->getId());
    }

    public function testGetName()
    {
        $author = new Author();
        $author->setName('John Doe');
        $this->assertEquals('john doe', $author->getName());
    }

    public function testGetAbout()
    {
        $author = new Author();
        $author->setAbout('Lorem ipsum dolor sit amet');
        $this->assertEquals('Lorem ipsum dolor sit amet', $author->getAbout());
    }

    public function testGetWebsite()
    {
        $author = new Author();
        $author->setWebsite('https://example.com');
        $this->assertEquals('https://example.com', $author->getWebsite());
    }

    public function testIsActive()
    {
        $author = new Author();
        $author->setActive(true);
        $this->assertTrue($author->getActive());
    }

    public function testGetAvatarName()
    {
        $author = new Author();
        $author->setAvatarName('avatar.jpg');
        $this->assertEquals('avatar.jpg', $author->getAvatarName());
    }

    public function testGetVideos()
    {
        $author = new Author();
        $video1 = new Video();
        $video2 = new Video();
        $author->addVideo($video1);
        $author->addVideo($video2);
        $this->assertCount(2, $author->getVideos());
        $this->assertTrue($author->getVideos()->contains($video1));
        $this->assertTrue($author->getVideos()->contains($video2));
        $author->removeVideo($video1);
        $this->assertCount(1, $author->getVideos());
        $this->assertFalse($author->getVideos()->contains($video1));
        $this->assertTrue($author->getVideos()->contains($video2));
    }
}