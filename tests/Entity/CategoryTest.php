<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Video;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testGetId()
    {
        $category = new Category();
        $this->assertNull($category->getId());
    }

    public function testGetName()
    {
        $category = new Category();
        $category->setName('Test Category');
        $this->assertEquals('test category', $category->getName());
    }

    public function testGetSlug()
    {
        $category = new Category();
        $category->setSlug('test-category');
        $this->assertEquals('test-category', $category->getSlug());
    }

    public function testGetColor()
    {
        $category = new Category();
        $category->setColor('black');
        $this->assertEquals('black', $category->getColor());
    }

    public function testGetPosition()
    {
        $category = new Category();
        $category->setPosition(1);
        $this->assertEquals(1, $category->getPosition());
    }

    public function testGetImageName()
    {
        $category = new Category();
        $category->setImageName('test.jpg');
        $this->assertEquals('test.jpg', $category->getImageName());
    }

    public function testGetDescription()
    {
        $category = new Category();
        $category->setDescription('Lorem ipsum dolor sit amet');
        $this->assertEquals('Lorem ipsum dolor sit amet', $category->getDescription());
    }

    public function testIsActive()
    {
        $category = new Category();
        $category->setActive(true);
        $this->assertTrue($category->getActive());
    }

    public function testGetVideos()
    {
        $category = new Category();
        $video1 = new Video();
        $video2 = new Video();
        $category->addVideo($video1);
        $category->addVideo($video2);
        $this->assertCount(2, $category->getVideos());
        $this->assertTrue($category->getVideos()->contains($video1));
        $this->assertTrue($category->getVideos()->contains($video2));
        $category->removeVideo($video1);
        $this->assertCount(1, $category->getVideos());
        $this->assertFalse($category->getVideos()->contains($video1));
        $this->assertTrue($category->getVideos()->contains($video2));
    }
}