<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $user = new User();

        $this->assertNull($user->getId());

        $user->setEmail('test@example.com');
        $this->assertSame('test@example.com', $user->getEmail());

        $user->setFullname('John Doe');
        $this->assertSame('John Doe', $user->getFullname());

        $user->setIsVisitor(true);
        $this->assertTrue($user->getIsVisitor());

        $user->setActive(true);
        $this->assertTrue($user->getActive());

        $user->setRoles(['ROLE_ADMIN']);
        $this->assertSame(['ROLE_ADMIN', 'ROLE_USER'], $user->getRoles());

        $user->setPassword('password');
        $this->assertSame('password', $user->getPassword());

        $user->setPlainPassword('new_password');
        $this->assertSame('new_password', $user->getPlainPassword());
    }

    public function testUserIdentifier(): void
    {
        $user = new User();
        $user->setEmail('test@example.com');

        $this->assertSame('test@example.com', $user->getUserIdentifier());
    }
}