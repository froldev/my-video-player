<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Trait\CreatedAtTrait;
use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class VideoFixtures extends Fixture
{
    use CreatedAtTrait;

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $videos = [
            [
                'title' => 'Découverte et démarrage du projet FreeReads',
                'slug' => 'decouverte-et-demarrage-du-projet-freereads',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=OjGkvDtdMe0',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Mise en place des outils',
                'slug' => 'mise-en-place-des-outils',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=APW0mWLJnE8',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Conception du modèle de données',
                'slug' => 'conception-du-modele-de-donnees',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=wcEHfXsj8mo',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Création des entités et des tests unitaires',
                'slug' => 'creation-des-entites-et-des-tests-unitaires',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=DpTLdBy6Mtw',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Migration Symfony 6 et Fixtures',
                'slug' => 'migration-symfony-6-et-fixtures',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=6CAWjjjKvbc',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Gagner du temps avec EasyAdmin',
                'slug' => 'gagner-du-temps-avec-easyadmin',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=4dmsUalc5Ds',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Inscriptions sur invitations',
                'slug' => 'inscriptions-sur-invitations',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=qnQZwtHKP44',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Sécurité : User & Hachage de mot de passe',
                'slug' => 'securite-user-et-hachage-de-mot-de-passe',
                'category' => CategoryFixtures::SYMFONY,
                'url' => 'https://www.youtube.com/watch?v=dODeLDHSnlQ&list=PLUiuGjup8Vg5t20nu7aaJDnbHlhzXbbuN&index=12',
                'author' => AuthorFixtures::DEVELOPPEUR_MUSCLE,
            ],
            [
                'title' => 'Tutoriel VueJS : Vue.js 3.2',
                'slug' => 'tutoriel-vusjs-vusjs-3-2',
                'category' => CategoryFixtures::VUEJS,
                'url' => 'https://www.youtube.com/watch?v=TWE6NVVPNcc',
                'author' => AuthorFixtures::GRAFIKART,
            ],
            [
                'title' => 'Tout comprendre de Vue.js 3.0.0',
                'slug' => 'tout-comprendre-de-vuejs-3-0-0',
                'category' => CategoryFixtures::VUEJS,
                'url' => 'https://www.youtube.com/watch?v=CzoIcHfW1fQ',
                'author' => AuthorFixtures::DEVTHEORY,
            ],
            [
                'title' => 'DOCKERISER une application SYMFONY',
                'slug' => 'dockeriser-une-application-symfony',
                'category' => CategoryFixtures::DOCKER,
                'url' => 'https://www.youtube.com/watch?v=KFWnB5hW6j8',
                'author' => AuthorFixtures::YOANDEV,
            ],
            [
                'title' => 'Apprendre Vue.js en 40 minutes',
                'slug' => 'apprendre-vuejs-en-40-minutes',
                'category' => CategoryFixtures::VUEJS,
                'url' => 'https://www.youtube.com/watch?v=DcEkrjdQW-8',
                'author' => AuthorFixtures::LARAVEL_JUTSU,
            ],
        ];

        // Videos
        foreach ($videos as $key => $value) {
            $category = $manager->getRepository(Category::class)->findOneBy(['name' => $value['category']]);
            $author = $manager->getRepository(Author::class)->findOneBy(['name' => $value['author']]);

            $video = new Video();
            $video->setTitle($value['title'])
                ->setSlug($value['slug'])
                ->setUrl($value['url'])
                ->setDescription($faker->text())
                ->setHomepage(rand(0, 1))
                ->setActive(true)
                ->setCategory($category)
                ->setAuthor($author)
                ->setCreatedAt(new \DateTimeImmutable())
            ;
            $manager->persist($video);
        }

        $manager->flush();
    }
}
