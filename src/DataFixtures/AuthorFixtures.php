<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Trait\CreatedAtTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AuthorFixtures extends Fixture
{
    use CreatedAtTrait;

    // AUTHORS
    public const YOANDEV = 'YoanDev';
    public const DEVELOPPEUR_MUSCLE = 'Développeur Musclé';
    public const GRAFIKART = 'Grafikart';
    public const DEVTHEORY = 'DevTheory';
    public const LARAVEL_JUTSU = 'Laravel Jutsu';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $authors = [
            self::YOANDEV => [
                'name' => self::YOANDEV,
                'slug' => 'yoandev',
                'website' => 'https://yoandev.co/',
                'avatar' => 'avatar_yoandev.jpg',
            ],
            self::DEVELOPPEUR_MUSCLE => [
                'name' => self::DEVELOPPEUR_MUSCLE,
                'slug' => 'developpeur-muscle',
                'website' => '',
                'avatar' => 'avatar_developpeur_muscle.jpg',
            ],
            self::GRAFIKART => [
                'name' => self::GRAFIKART,
                'slug' => 'grafikart',
                'website' => 'https://grafikart.fr/',
                'avatar' => 'avatar_grafikart.jpg',
            ],
            self::DEVTHEORY => [
                'name' => self::DEVTHEORY,
                'slug' => 'devtheory',
                'website' => 'https://devtheory.fr/',
                'avatar' => 'avatar_devtheory.jpg',
            ],
            self::LARAVEL_JUTSU => [
                'name' => self::LARAVEL_JUTSU,
                'slug' => 'laravel-jutsu',
                'website' => 'https://www.laraveljutsu.net/',
                'avatar' => 'avatar_laravel_jutsu.jpg',
            ],
        ];

        // Authors
        foreach ($authors as $value) {
            $author = new Author();
            $author
                ->setName($value['name'])
                ->setSlug($value['slug'])
                ->setAbout(mt_rand(0, 1) ? $faker->paragraphs(3, true) : null)
                ->setWebsite($value['website'])
                ->setActive(true)
                ->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($author);
        }

        $manager->flush();
    }
}
