<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Trait\CreatedAtTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    use CreatedAtTrait;

    // CATEGORIES
    public const SYMFONY = 'Symfony';
    public const VUEJS = 'VueJS';
    public const DOCKER = 'Docker';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $categories = [
            [
                'name' => self::SYMFONY,
                'slug' => 'symfony',
                'color' => 'white',
                'background' => 'black',
                'banner' => 'banner_symfony.jpg',
            ],
            [
                'name' => self::VUEJS,
                'slug' => 'vuejs',
                'color' => 'green',
                'background' => 'white',
                'banner' => 'banner_vuejs.jpg',
            ],
            [
                'name' => self::DOCKER,
                'slug' => 'docker',
                'color' => 'white',
                'background' => 'orange',
                'banner' => 'banner_docker.jpg',
            ],
        ];

        // Categories
        foreach ($categories as $key => $value) {
            $category = new Category();
            $category->setName($value['name'])
            ->setSlug(strtolower($value['slug']))
            ->setPosition($key)
            ->setColor($value['color'])
            ->setBackground($value['background'])
            ->setDescription($faker->text())
            ->setActive(true)
            ->setCreatedAt(new \DateTimeImmutable())
            ;
            $manager->persist($category);
        }

        $manager->flush();
    }
}
