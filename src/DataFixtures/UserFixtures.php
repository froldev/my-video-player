<?php

namespace App\DataFixtures;

use App\Entity\Trait\CreatedAtTrait;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    use CreatedAtTrait;

    // ADMIN
    public const ADMIN_EMAIL = 'admin@admin.fr';
    public const ADMIN_PASSWORD = 'password';

    public function load(ObjectManager $manager): void
    {
        // admin
        $user = new User();
        $user->setEmail('administrateur@myvideoplayer.fr')
            ->setPlainPassword('password')
            ->setFullname('administrateur')
            ->setRoles(['ROLE_ADMIN'])
            ->setActive(true)
        ;
        $manager->persist($user);

        // user
        $user = new User();
        $user->setEmail('utilisateur@myvideoplayer.fr')
            ->setPlainPassword('visiteur')
            ->setFullname('utilisateur')
            ->setRoles(['ROLE_USER'])
            ->setIsVisitor(true)
            ->setActive(true)
        ;
        $manager->persist($user);

        $manager->flush();
    }
}
