<?php

namespace App\Controller;

use App\Entity\Video;
use App\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VideoController extends AbstractController
{
    public function __construct(
        private readonly AuthorRepository $authorRepository,
    ) {
    }

    #[Route('/video/{slug}', name: 'app_video_show')]
    public function index(Video $video): Response
    {
        return $this->render('pages/video/index.html.twig', [
            'video' => $video,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
        ]);
    }

    #[Route('/video', name: 'app_video_redirect')]
    public function video(): Response
    {
        return $this->redirectToRoute('app_home');
    }
}
