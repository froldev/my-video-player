<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;

class LoginController extends AbstractController
{
    #[Route('/connexion', name: 'app_login', methods: ['GET', 'POST'])]
    public function index(AuthenticationUtils $authenticationUtils, UserRepository $userRepository): Response
    {
        return $this->render('pages/login/index.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    #[Route(path: '/deconnexion', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank');
    }

    #[Route(path: '/visitor', name: 'app_visitor')]
    public function visitor(UserRepository $userRepository, LoginLinkHandlerInterface $loginLink): Response
    {
        $visitor = $userRepository->getVisitor();

        if (null === $visitor) {
            return $this->redirectToRoute('app_login');
        }
        $loginLinkDetails = $loginLink->createLoginLink($visitor);

        return $this->redirect($loginLinkDetails->getUrl());
    }
}
