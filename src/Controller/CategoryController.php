<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\AuthorRepository;
use App\Repository\CategoryRepository;
use App\Repository\VideoRepository;
use App\Service\PaginationService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    public function __construct(
        private readonly VideoRepository $videoRepository,
        private readonly AuthorRepository $authorRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly PaginationService $paginationService,
    ) {
    }

    #[Route('/categorie/{slug}/search', name: 'app_category_search', methods: ['POST'])]
    public function search(Category $category, Request $request): Response
    {
        $search = $request->request->get('search');

        return $this->render('components/_videos.html.twig', [
            'videos' => (!empty($search)) ? $this->paginationService->getPaginatedVideos($search, $category, false) : $this->paginationService->getPaginatedVideos(null, $category, false),
            'video_description_length' => $this->videoRepository::VIDEO_DESCRIPTION_LENGTH,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
            'banner_image_default' => $this->categoryRepository::DEFAULT_IMAGE_BANNER,
        ]);
    }

    #[Route('/categorie/{slug}', name: 'app_category_show')]
    public function show(Category $category, Request $request): Response
    {
        return $this->render('pages/category/category.html.twig', [
            'category' => $category,
            'videos' => $this->paginationService->getPaginatedVideos(null, $category, false),
            'category_description_length' => CategoryRepository::CATEGORY_DESCRIPTION_LENGTH,
            'video_description_length' => VideoRepository::VIDEO_DESCRIPTION_LENGTH,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
            'banner_image_default' => $this->categoryRepository::DEFAULT_IMAGE_BANNER,
        ]);
    }

    #[Route('/category', name: 'app_category_redirect')]
    public function category(): Response
    {
        return $this->redirectToRoute('app_category_all');
    }

    #[Route('/categories', name: 'app_category_all')]
    public function index(
        CategoryRepository $categoryRepository,
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $categories = $paginator->paginate(
            $categoryRepository->findBy(['active' => true], [
                'position' => 'ASC',
            ]),
            $request->query->getInt('page', 1),
            VideoRepository::MAX_VIDEOS_PER_PAGE,
        );

        return $this->render('pages/category/allCategories.html.twig', [
            'categories' => $categories,
        ]);
    }
}
