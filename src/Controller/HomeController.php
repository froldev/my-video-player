<?php

namespace App\Controller;

use App\Repository\AuthorRepository;
use App\Repository\CategoryRepository;
use App\Repository\VideoRepository;
use App\Service\PaginationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(
        private readonly VideoRepository $videoRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly AuthorRepository $authorRepository,
        private readonly PaginationService $paginationService,
    ) {
    }

    #[Route('/search', name: 'app_home_search')]
    public function search(Request $request): Response
    {
        $search = $request->request->get('search');

        return $this->render('components/_videos.html.twig', [
            'videos' => (!empty($search)) ? $this->paginationService->getPaginatedVideos($search, null, true) : $this->paginationService->getPaginatedVideos(null, null, true),
            'video_description_length' => $this->videoRepository::VIDEO_DESCRIPTION_LENGTH,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
        ]);
    }

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('pages/home/index.html.twig', [
            'videos' => $this->paginationService->getPaginatedVideos(null, null, true),
            'video_description_length' => $this->videoRepository::VIDEO_DESCRIPTION_LENGTH,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
        ]);
    }

    public function renderNavBar(): Response
    {
        return $this->render('partials/_navbar.html.twig', [
            'categories' => $this->categoryRepository->findBy(['active' => true], [
                'position' => 'ASC',
            ]),
            'max' => $this->categoryRepository::MAX_LINKS_NAV,
        ]);
    }

    public function renderFooter(): Response
    {
        return $this->render('partials/_footer.html.twig', [
            'categories' => $this->categoryRepository->findBy(['active' => true], [
                'position' => 'ASC',
            ]),
            'authors' => $this->authorRepository->findBy(['active' => true], [
                'name' => 'ASC',
            ]),
            'max' => $this->categoryRepository::MAX_LINKS_NAV,
        ]);
    }
}
