<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use App\Repository\VideoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    public function __construct(
        private readonly VideoRepository $videoRepository,
        private readonly AuthorRepository $authorRepository,
    ) {
    }

    #[Route('/auteur/{slug}', name: 'app_author_show')]
    public function show(
        Author $author,
        Request $request,
        PaginatorInterface $paginator,
    ): Response {
        $videos = $paginator->paginate(
            $this->videoRepository->findVideosByAuthor($author),
            $request->query->getInt('page', 1),
            VideoRepository::MAX_VIDEOS_PER_PAGE,
        );

        return $this->render('pages/author/author.html.twig', [
            'author' => $author,
            'videos' => $videos,
            'video_description_length' => VideoRepository::VIDEO_DESCRIPTION_LENGTH,
            'author_image_default' => $this->authorRepository::DEFAULT_IMAGE_AUTHOR,
        ]);
    }

    #[Route('/auteurs', name: 'app_author_all')]
    public function index(
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $authors = $paginator->paginate(
            $this->authorRepository->findBy(['active' => true], [
                'name' => 'ASC',
            ]),
            $request->query->getInt('page', 1),
            AuthorRepository::MAX_AUTHORS_PER_PAGE,
        );

        return $this->render('pages/author/allAuthors.html.twig', [
            'authors' => $authors,
        ]);
    }
}
