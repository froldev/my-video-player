<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Response;
use Vich\UploaderBundle\Form\Type\VichImageType;

enum Direction
{
    case Top;
    case Up;
    case Down;
    case Bottom;
}

class CategoryCrudController extends AbstractCrudController
{
    public const ACTION_MOVE_TOP = 'moveTop';
    public const ACTION_MOVE_UP = 'moveUp';
    public const ACTION_MOVE_DOWN = 'moveDown';
    public const ACTION_MOVE_BOTTOM = 'moveBottom';
    public const CATEGORY_BASE_PATH = '/uploads/categories';
    public const CATEGORY_UPLOAD_DIR = 'public/uploads/categories';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly CategoryRepository $categoryRepository,
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $entityCount = $this->categoryRepository->count([]);

        $moveTop = Action::new(self::ACTION_MOVE_TOP, false, 'fa fa-arrow-up')
            ->setHtmlAttributes(['title' => 'Tout en Haut'])
            ->linkToCrudAction('moveTop')
            ->displayIf(fn ($entity) => $entity->getPosition() > 0);

        $moveUp = Action::new(self::ACTION_MOVE_UP, false, 'fa fa-sort-up')
            ->setHtmlAttributes(['title' => 'Monter'])
            ->linkToCrudAction('moveUp')
            ->displayIf(fn ($entity) => $entity->getPosition() > 0);

        $moveDown = Action::new(self::ACTION_MOVE_DOWN, false, 'fa fa-sort-down')
            ->setHtmlAttributes(['title' => 'Descendre'])
            ->linkToCrudAction('moveDown')
            ->displayIf(fn ($entity) => $entity->getPosition() < $entityCount - 1);

        $moveBottom = Action::new(self::ACTION_MOVE_BOTTOM, false, 'fa fa-arrow-down')
            ->setHtmlAttributes(['title' => 'Tout en Bas'])
            ->linkToCrudAction('moveBottom')
            ->displayIf(fn ($entity) => $entity->getPosition() < $entityCount - 1);

        return $actions
            ->add(Crud::PAGE_INDEX, $moveBottom)
            ->add(Crud::PAGE_INDEX, $moveDown)
            ->add(Crud::PAGE_INDEX, $moveUp)
            ->add(Crud::PAGE_INDEX, $moveTop)
            ->setPermission(self::ACTION_MOVE_TOP, 'ROLE_ADMIN')
            ->setPermission(self::ACTION_MOVE_UP, 'ROLE_ADMIN')
            ->setPermission(self::ACTION_MOVE_DOWN, 'ROLE_ADMIN')
            ->setPermission(self::ACTION_MOVE_BOTTOM, 'ROLE_ADMIN')
        ;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Catégories')
            ->setPageTitle('edit', 'Editer une catégorie')
            ->setPageTitle('new', 'Créer une catégorie')
            ->setDefaultSort(['position' => 'ASC'])
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield NumberField::new('position', 'Position')
            ->hideOnForm();

        yield TextField::new('name', 'Nom');

        yield SlugField::new('slug')
            ->setTargetFieldName('name')
            ->hideOnIndex();

        yield ChoiceField::new('background', 'Fond du badge')->setChoices([
            'Noir' => 'black',
            'Blanc' => 'white',
            'Gris' => 'grey',
            'Bleu' => 'blue',
            'Rouge' => 'red',
            'Vert' => 'green',
            'Orange' => 'orange',
            'Jaune' => 'yellow',
            'Violet' => 'purple',
            'Rose' => 'pink',
        ]);

        yield ChoiceField::new('color', 'Texte du badge')->setChoices([
            'Noir' => 'black',
            'Blanc' => 'white',
            'Gris' => 'grey',
            'Bleu' => 'blue',
            'Rouge' => 'red',
            'Vert' => 'green',
            'Orange' => 'orange',
            'Jaune' => 'yellow',
            'Violet' => 'purple',
            'Rose' => 'pink',
        ]);

        yield TextEditorField::new('description')
            ->hideOnIndex();

        yield TextField::new('imageFile')
            ->setFormType(VichImageType::class)
            ->onlyOnForms();

        yield ImageField::new('imageName', 'Image')
            ->setBasePath(self::CATEGORY_BASE_PATH)
            ->setUploadDir(self::CATEGORY_UPLOAD_DIR)
            ->onlyOnIndex();

        yield BooleanField::new('active', 'Publiée');
    }

    public function moveTop(AdminContext $context): Response
    {
        return $this->move($context, Direction::Top);
    }

    public function moveUp(AdminContext $context): Response
    {
        return $this->move($context, Direction::Up);
    }

    public function moveDown(AdminContext $context): Response
    {
        return $this->move($context, Direction::Down);
    }

    public function moveBottom(AdminContext $context): Response
    {
        return $this->move($context, Direction::Bottom);
    }

    private function move(AdminContext $context, Direction $direction): Response
    {
        $object = $context->getEntity()->getInstance();
        $newPosition = match ($direction) {
            Direction::Top => 0,
            Direction::Up => $object->getPosition() - 1,
            Direction::Down => $object->getPosition() + 1,
            Direction::Bottom => -1,
        };

        $object->setPosition($newPosition);
        $this->em->flush();

        $this->addFlash('success', 'La Catégorie a été déplacée avec succès.');

        return $this->redirect($context->getReferrer());
    }
}
