<?php

namespace App\Controller\Admin;

use App\Entity\Author;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AuthorCrudController extends AbstractCrudController
{
    public const AUTHOR_BASE_PATH = '/uploads/authors';
    public const AUTHOR_UPLOAD_DIR = 'public/uploads/authors';

    public static function getEntityFqcn(): string
    {
        return Author::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Auteurs')
            ->setPageTitle('edit', 'Editer un auteur')
            ->setPageTitle('new', 'Créer un auteur')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Nom');

        yield SlugField::new('slug', 'Url')
            ->setTargetFieldName('name')
            ->hideOnIndex();

        yield TextEditorField::new('about', 'A propos');
        yield TextField::new('website', 'Site de l\'auteur');

        yield TextField::new('avatarFile', 'Fichier')
            ->setFormType(VichImageType::class)
            ->onlyOnForms();

        yield ImageField::new('avatarName', 'Image')
            ->setBasePath(self::AUTHOR_BASE_PATH)
            ->setUploadDir(self::AUTHOR_UPLOAD_DIR)
            ->onlyOnIndex();

        yield BooleanField::new('active', 'Publié');
    }
}
