<?php

namespace App\Controller\Admin;

use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;

class VideoCrudController extends AbstractCrudController
{
    public const ACTION_DUPLICATE = 'duplicate';

    public static function getEntityFqcn(): string
    {
        return Video::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $duplicate = Action::new(self::ACTION_DUPLICATE, 'Dupliquer')
            ->linkToCrudAction('duplicateVideo')
            ->setCssClass('btn btn-success')
            ->setIcon('fa fa-copy');

        return parent::configureActions($actions)
            ->add(Crud::PAGE_EDIT, $duplicate);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Vidéos')
            ->setPageTitle('edit', 'Editer une vidéo')
            ->setPageTitle('new', 'Créer une vidéo')
            ->setPageTitle('detail', 'Consulter une vidéo')
            ->showEntityActionsInlined();
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('category', 'Catégorie')
            ->setQueryBuilder(function (QueryBuilder $queryBuilder) {
                $queryBuilder
                    ->where('entity.active = true')
                    ->orderBy('entity.name', 'ASC');
            });

        yield AssociationField::new('author', 'Auteur')
            ->setQueryBuilder(function (QueryBuilder $queryBuilder) {
                $queryBuilder
                ->where('entity.active = true')
                ->orderBy('entity.name', 'ASC');
            });

        yield TextField::new('title', 'Titre');

        yield SlugField::new('slug')
            ->setTargetFieldName('title')
            ->hideOnIndex();

        yield TextField::new('url', 'URL')
            ->hideOnIndex();

        yield TextEditorField::new('description', 'Description')
            ->hideOnIndex();

        yield BooleanField::new('active', 'Publiée');
        yield BooleanField::new('homepage', 'Mise en avant');
    }

    public function duplicateVideo(
        AdminContext $context,
        AdminUrlGenerator $adminUrlGenerator,
        EntityManagerInterface $em
    ): Response {
        /** @var Video $video */
        $video = $context->getEntity()->getInstance();

        $duplicatedVideo = clone $video;

        parent::persistEntity($em, $duplicatedVideo);

        $url = $adminUrlGenerator->setController(self::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($duplicatedVideo->getId())
            ->generateUrl();

        return $this->redirect($url);
    }
}
