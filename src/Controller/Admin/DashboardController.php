<?php

namespace App\Controller\Admin;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\Video;
use App\Repository\AuthorRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public const ACTION_CANCEL = 'cancel';

    private const AUTHOR = 'author';
    private const AUTHORS = 'authors';
    private const CATEGORY = 'category';
    private const CATEGORIES = 'categories';
    private const TOTAL_PUBLISHED = 'totalPublished';
    private const TOTAL_UNPUBLISHED = 'totalUnpublished';
    private const TOTAL = 'total';
    private const PUBLISHED = 'published';
    private const UNPUBLISHED = 'unpublished';
    private const ACTIVE = 'active';
    private const NB_VIDEOS = 'nbVideos';
    private const POURCENTAGE = 'pourcentage';
    private const HOMEPAGE = 'homepage';
    private const VIDEOS = 'videos';
    private const ALL_VIDEOS = 'allVideos';
    private const NAME = 'name';
    private const USER = 'user';
    private const USERS = 'users';

    public function __construct(
        private readonly VideoRepository $videoRepository,
        private readonly UserRepository $userRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly AuthorRepository $authorRepository,
        private $datas = [],
        private $videos = [],
        private $users = [],
        private $categories = [],
        private $authors = [],
        private $totalVideosHomepage = 0,
        private $totalAllVideos = 0,
        private $totalCategories = 0,
        private $totalAuthors = 0,
        private $totalUsers = 0,
        private $publishedHomepage = 0,
        private $publishedAllVideos = 0,
        private $publishedCategories = 0,
        private $publishedAuthors = 0,
        private $publishedUsers = 0,
        private $unpublishedHomepage = 0,
        private $unpublishedAllVideos = 0,
        private $unpublishedCategories = 0,
        private $unpublishedAuthors = 0,
        private $unpublishedUsers = 0,
        private $allActive = false,
    ) {
    }

    #[Route('/dashboard', name: 'admin_dashboard')]
    public function index(): Response
    {
        $this->videos = $this->videoRepository->getVideosForDashboard();
        $this->categories = $this->categoryRepository->getCategoriesForDashboard();
        $this->authors = $this->authorRepository->getAuthorsForDashboard();
        $this->users = $this->userRepository->getUsersForDashboard();

        // count total videos and total videos homepage
        $this->countTotal();

        // create array datas
        $this->createOriginArrayDatas();

        // Videos
        $this->addDatasVideos();

        // Categories
        $this->addDatasCategories();

        // Authors
        $this->addDatasAuthors();

        // Users
        $this->addDatasUsers();

        return $this->render('admin/dashboard.html.twig', [
            'datas' => $this->datas,
        ]);
    }

    /**
     * Count total.
     */
    private function countTotal(): void
    {
        foreach ($this->videos as $video) {
            // Homepage
            if ($video['homepage']) {
                ++$this->totalVideosHomepage;
                if ($video['video_active'] && $video['category_active'] && $video['author_active']) {
                    ++$this->publishedHomepage;
                } else {
                    ++$this->unpublishedHomepage;
                }
            }

            // All Videos
            if ($video['video_active'] && $video['category_active'] && $video['author_active']) {
                ++$this->publishedAllVideos;
            } else {
                ++$this->unpublishedAllVideos;
            }
        }

        // Categories
        foreach ($this->categories as $category) {
            ($category['active']) ? ++$this->publishedCategories : ++$this->unpublishedCategories;
        }

        // Authors
        foreach ($this->authors as $author) {
            ($author['active']) ? ++$this->publishedAuthors : ++$this->unpublishedAuthors;
        }

        // Users
        foreach ($this->users as $user) {
            ($user['active']) ? ++$this->publishedUsers : ++$this->unpublishedUsers;
        }
    }

    /**
     * Create origin array datas.
     */
    private function createOriginArrayDatas(): void
    {
        $this->datas = [
            self::HOMEPAGE => [
                self::VIDEOS => [],
                self::TOTAL_PUBLISHED => $this->publishedHomepage,
                self::TOTAL_UNPUBLISHED => $this->unpublishedHomepage,
                self::TOTAL => $this->totalVideosHomepage,
            ],
            self::ALL_VIDEOS => [
                self::VIDEOS => [],
                self::TOTAL_PUBLISHED => $this->publishedAllVideos,
                self::TOTAL_UNPUBLISHED => $this->unpublishedAllVideos,
                self::TOTAL => count($this->videos),
            ],
            self::CATEGORY => [
                self::CATEGORIES => [],
                self::TOTAL_PUBLISHED => $this->publishedCategories,
                self::TOTAL_UNPUBLISHED => $this->unpublishedCategories,
                self::TOTAL => count($this->categories),
            ],
            self::AUTHOR => [
                self::AUTHORS => [],
                self::TOTAL_PUBLISHED => $this->publishedAuthors,
                self::TOTAL_UNPUBLISHED => $this->unpublishedAuthors,
                self::TOTAL => count($this->authors),
            ],
            self::USER => [
                self::USERS => [],
                self::TOTAL_PUBLISHED => $this->publishedUsers,
                self::TOTAL_UNPUBLISHED => $this->unpublishedUsers,
                self::TOTAL => count($this->users),
            ],
        ];
    }

    /**
     * Add datas videos.
     */
    private function addDatasVideos()
    {
        foreach ($this->videos as $video) {
            // verify if video and category and author are active
            $this->allActive = false;
            if ($video['video_active'] && $video['category_active'] && $video['author_active']) {
                $this->allActive = true;
            }

            // Homepage
            if ($video['homepage']) {
                $this->addDatasInArray(
                    self::HOMEPAGE,
                    self::VIDEOS,
                    $video['slug'],
                    $video['title'],
                    $video['author'],
                    $video['category'],
                    $this->allActive,
                    $this->datas[self::HOMEPAGE][self::TOTAL]
                );
            }

            // Videos
            $this->addDatasInArray(
                self::ALL_VIDEOS,
                self::VIDEOS,
                $video['slug'],
                $video['title'],
                $video['author'],
                $video['category'],
                $this->allActive,
                $this->datas[self::ALL_VIDEOS][self::TOTAL]
            );
        }
    }

    /**
     * Add datas categories.
     */
    private function addDatasCategories()
    {
        foreach ($this->categories as $category) {
            $this->addDatasInArray(
                self::CATEGORY,
                self::CATEGORIES,
                $category['slug'],
                $category['name'],
                null,
                null,
                $category['active'],
                $this->datas[self::CATEGORY][self::TOTAL_PUBLISHED]
            );
        }
    }

    /**
     * Add datas authors.
     */
    private function addDatasAuthors()
    {
        foreach ($this->authors as $author) {
            $this->addDatasInArray(
                self::AUTHOR,
                self::AUTHORS,
                $author['slug'],
                $author['name'],
                null,
                null,
                $author['active'],
                $this->datas[self::AUTHOR][self::TOTAL_PUBLISHED]
            );
        }
    }

    /**
     * Add datas users.
     */
    private function addDatasUsers()
    {
        foreach ($this->users as $user) {
            $this->addDatasInArray(
                self::USER,
                self::USERS,
                $this->slugify($user['fullname']),
                $user['fullname'],
                null,
                null,
                $user['active'],
                $this->datas[self::USER][self::TOTAL_PUBLISHED]
            );
        }
    }

    /**
     * Add datas in array.
     */
    private function addDatasInArray(
        string $level,
        string $sublevel,
        string $slug,
        string $name,
        ?string $author,
        ?string $category,
        bool $active,
        int $total,
    ): void {
        if (empty($this->datas[$level][$sublevel]) || !empty($this->datas[$level][$sublevel]) && !array_key_exists($slug, $this->datas[$level][$sublevel])) {
            $this->datas[$level][$sublevel][$slug] = [
                self::NAME => $name,
                self::CATEGORY => $category,
                self::AUTHOR => $author,
                self::ACTIVE => $active,
                self::NB_VIDEOS => 1,
                self::PUBLISHED => ($active) ? 1 : 0,
                self::UNPUBLISHED => (!$active) ? 1 : 0,
                self::POURCENTAGE => ($active) ? $this->getPourcentage($total, 1) : 0,
            ];
        } elseif (!empty($this->datas[$level][$sublevel]) && array_key_exists($slug, $this->datas[$level][$sublevel])) {
            ++$this->datas[$level][$sublevel][$slug][self::NB_VIDEOS];
            if ($this->allActive) {
                ++$this->datas[$level][$sublevel][$slug][self::PUBLISHED];
            } else {
                ++$this->datas[$level][$sublevel][$slug][self::UNPUBLISHED];
            }
            $this->datas[$level][$sublevel][$slug][self::POURCENTAGE] = $this->getPourcentage($total, $this->datas[$level][$sublevel][$slug][self::PUBLISHED]);
        }
    }

    /**
     * Get pourcentage.
     */
    private static function getPourcentage(int $total, int $nb): float
    {
        return round(($nb / $total) * 100, 2);
    }

    /**
     * Slugify.
     */
    private static function slugify($text, string $divider = '_'): string
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function configureActions(): Actions
    {
        $cancel = Action::new('cancel', 'Annuler')
            ->linkToCrudAction(Crud::PAGE_INDEX)
            ->setCssClass('btn btn-default')
            ->setIcon('fa fa-xmark');

        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, $cancel)
            ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action->setIcon('fa fa-plus')->setLabel('Ajouter');
            })
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pen-to-square')->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel(false);
            })
            ->setPermission(Action::NEW, 'ROLE_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_ADMIN')
            ->setPermission(Action::DELETE, 'ROLE_ADMIN');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('My Video Player')
            ->setLocales(['fr'])
            ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Tableau de bord', 'fa fa-home');

        // Entités
        yield MenuItem::section('Entités');
        yield MenuItem::linkToCrud('Catégories', 'fas fa-list', Category::class);
        yield MenuItem::linkToCrud('Auteurs', 'fa-solid fa-user', Author::class);
        yield MenuItem::linkToCrud('Vidéos', 'fas fa-video', Video::class);

        // Utilisateurs
        yield MenuItem::section('Paramètres')
            ->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-cog', User::class)
            ->setPermission('ROLE_ADMIN');

        // Redirection
        yield MenuItem::section('Vérifications')
            ->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToUrl('Site', 'fas fa-globe', $this->generateUrl('app_home'))
                ->setPermission('ROLE_ADMIN');

        // Redirection
        yield MenuItem::section('Actions');
        yield MenuItem::linkToLogout('Déconnexion', 'fas fa-right-from-bracket');
    }
}
