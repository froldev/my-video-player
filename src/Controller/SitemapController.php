<?php

namespace App\Controller;

use App\Repository\AuthorRepository;
use App\Repository\CategoryRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    public function __construct(
        private readonly VideoRepository $videoRepository,
        private readonly AuthorRepository $authorRepository,
        private readonly CategoryRepository $categoryRepository,
    ) {
    }

    #[Route('/sitemap.xml', name: 'app_sitemap', defaults: ['_format' => 'xml'])]
    public function index(Request $request): Response
    {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('app_home')];

        // categories
        foreach ($this->categoryRepository->findAll() as $category) {
            $urls[] = [
                'loc' => $this->generateUrl('app_category_show', ['slug' => $category->getSlug()]),
                'lastmod' => $category->getUpdatedAt()->format('Y-m-d'),
            ];
        }

        // authors
        foreach ($this->authorRepository->findAll() as $author) {
            $urls[] = [
                'loc' => $this->generateUrl('app_author_show', ['slug' => $author->getSlug()]),
                'lastmod' => $author->getUpdatedAt()->format('Y-m-d'),
            ];
        }

        // videos
        foreach ($this->videoRepository->findAll() as $video) {
            $urls[] = [
                'loc' => $this->generateUrl('app_video_show', ['slug' => $video->getSlug()]),
                'lastmod' => $video->getUpdatedAt()->format('Y-m-d'),
            ];
        }

        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname,
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
