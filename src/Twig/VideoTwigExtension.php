<?php

namespace App\Twig;

use MediaEmbed\MediaEmbed;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class VideoTwigExtension extends AbstractExtension
{
    public function __construct(
        private object $mediaEmbed = new MediaEmbed(),
    ) {
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/3.x/advanced.html#automatic-escaping
            new TwigFilter('video_thumbnail', [$this, 'videoThumbnail']),
            new TwigFilter('video_player', [$this, 'videoPlayer']),
        ];
    }

    public function videoThumbnail($value)
    {
        $video = $this->mediaEmbed->parseUrl($value);

        return $video->image();
    }

    public function videoPlayer($value)
    {
        $video = $this->mediaEmbed->parseUrl($value);

        return $video->getEmbedCode();
    }
}
