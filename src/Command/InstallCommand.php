<?php

namespace App\Command;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\Video;
use App\Service\EncodePasswordService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:install',
    description: 'Add users to the database',
)]
class InstallCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $entityManagerInterface,
        private EncodePasswordService $encodePassword,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $password = 'password';

        // create admin
        $user = new User();
        $user->setEmail('administrateur@myvideoplayer.fr')
            ->setPlainPassword($password)
            ->setFullname('administrateur')
            ->setRoles(['ROLE_ADMIN'])
            ->setActive(true)
        ;
        $this->encodePassword->encode($user);
        $this->entityManagerInterface->persist($user);

        // create visitor
        $user = new User();
        $user->setEmail('utilisateur@myvideoplayer.fr')
            ->setPlainPassword($password)
            ->setFullname('utilisateur')
            ->setRoles(['ROLE_USER'])
            ->setIsVisitor(true)
            ->setActive(true)
        ;
        $this->encodePassword->encode($user);
        $this->entityManagerInterface->persist($user);

        // create a category
        $category = new Category();
        $category->setName('Symfony')
            ->setSlug('symfony')
            ->setColor('white')
            ->setBackground('black')
            ->setPosition(0)
            ->setDescription("Symfony est un ensemble de composants PHP ainsi qu'un framework MVC libre écrit en PHP. Il fournit des fonctionnalités modulables et adaptables qui permettent de faciliter et d’accélérer le développement d'un site web.")
            ->setActive(true);
        $this->entityManagerInterface->persist($category);

        // create one author
        $author = new Author();
        $author->setName('Yoandev')
            ->setSlug('yoandev')
            ->setAbout("Symfony, Docker, DevOps ou encore GitLab sont des sujets qui vous intéresse ? Vous êtes au bon endroit !
            On aborde pleins de sujets sur le monde du PHP, du développement WEB, mais aussi de l'intégration continue, du DevOps ou de l'utilisation de Docker ! Je propose des tutoriels simple et accessible, en français !
            Étant moi-même un perpétuel débutant et en perpétuel apprentissage, j'essaye d'être le plus pédagogue possible afin que les tutoriels soient facile à comprendre ;-)")
            ->setWebsite('https://yoandev.co/')
            ->setActive(true);
        $this->entityManagerInterface->persist($author);

        // create one video
        $video = new Video();
        $video->setTitle('Un environnement de développement Symfony 5 avec Docker et Docker-compose')
            ->setSlug('Un-environnement-de-developpement-symfony-5-avec-docker-et-docker-compose')
            ->setUrl('https://www.youtube.com/watch?v=tRI6KFNKfFo')
            ->setDescription('Je ne sais pas vous, mais installer une base de données MySQL, un phpMyAdmin ou encore un Apache avec PHP, même avec des solutions packagées (WAMP, MAMP ou autres joyeusetés de ce genre) n’est pas ma plus grande passion ! Et pas envie de me prendre la tête à tout ré-installer si je change de machine !')
            ->setCategory($category)
            ->setAuthor($author)
            ->setHomepage(true)
            ->setActive(true);
        $this->entityManagerInterface->persist($video);

        $this->entityManagerInterface->flush();

        $io->success('Users admin, visitor, category Symfony, author Yoandev and one video are created !');

        return Command::SUCCESS;
    }
}
