<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Video>
 *
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public const VIDEO_DESCRIPTION_LENGTH = 150;
    public const MAX_VIDEOS_PER_PAGE = 6;

    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Video::class);
    }

    public function save(Video $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Video $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Find videos by category or all videos if no category is provided
     * with author is active.
     * with category is active and add.
     * with homepage is true.
     * with search in title, author name or category name.
     */
    public function findVideosByCategory(
        ?string $search,
        object $category = null,
        ?bool $homepage = false,
        int $limit = null
    ): Query {
        if ($category) {
            $query = $this->createQueryBuilder('v')
            ->leftJoin('v.category', 'c')
            ->leftJoin('v.author', 'a')
            ->where('v.active = true')
            ->andWhere('a.active = true')
            ->andWhere('c.slug = :slug')
            ->setParameter('slug', $category->getSlug())
            ->orderBy('v.createdAt', 'DESC');

            if ($search) {
                $query->andWhere('v.title LIKE :search OR a.name LIKE :search OR c.name LIKE :search')
                ->setParameter('search', '%'.$search.'%');
            }

            if ($homepage) {
                $query->andWhere('v.homepage = true');
            }

            if ($limit) {
                $query->setMaxResults($limit);
            }

            return $query->getQuery();
        }

        $query = $this->createQueryBuilder('v')
            ->leftJoin('v.author', 'a')
            ->where('v.active = true')
            ->andWhere('a.active = true')
            ->orderBy('v.createdAt', 'DESC');

        if ($search) {
            $query->andWhere('v.title LIKE :search OR a.name LIKE :search')
            ->setParameter('search', '%'.$search.'%');
        }

        if ($homepage) {
            $query->andWhere('v.homepage = true');
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery();
    }

    public function findVideosByAuthor(object $author): array
    {
        return $this->createQueryBuilder('v')
            ->leftJoin('v.author', 'a')
            ->where('a.slug = :slug')
            ->andWhere('v.active = true')
            ->andWhere('a.active = true')
            ->setParameter('slug', $author->getSlug())
            ->orderBy('v.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Return videos for dashboard.
     */
    public function getVideosForDashboard(): array
    {
        return $this->createQueryBuilder('v')
            ->leftJoin('v.category', 'c')
            ->leftJoin('v.author', 'a')
            ->select('v.title, v.slug, v.homepage, v.active AS video_active, c.name AS category, c.active AS category_active, a.name AS author, a.active AS author_active')
            ->orderBy('v.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
