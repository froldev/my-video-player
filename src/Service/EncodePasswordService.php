<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EncodePasswordService
{
    public function __construct(
        private UserPasswordHasherInterface $hasher
    ) {
        $this->hasher = $hasher;
    }

    /**
     * Encode password based on plain password.
     */
    public function encode(User $user): void
    {
        if (!$user->getPlainPassword()) {
            return;
        }

        $user->setPassword(
            $this->hasher->hashPassword(
                $user,
                $user->getPlainPassword()
            )
        );

        $user->setPlainPassword(null);
    }
}
