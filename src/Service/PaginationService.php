<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Category;
use App\Repository\VideoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PaginationService
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly VideoRepository $videoRepository,
        private readonly PaginatorInterface $paginatorInterface,
    ) {
    }

    public function getPaginatedVideos(string $search = null, Category $category = null, $homepage = false)
    {
        $request = $this->requestStack->getMainRequest();
        $page = $request->query->getInt('page', 1);

        if ($search) {
            $search = strtolower($search);
        }

        $videosQuery = $this->videoRepository->findVideosByCategory($search, $category, $homepage);

        return $this->paginatorInterface->paginate(
            $videosQuery,
            $page,
            $this->videoRepository::MAX_VIDEOS_PER_PAGE,
        );
    }
}
