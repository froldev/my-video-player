<?php

namespace App\EventSubscriber;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\Video;
use App\Service\EncodePasswordService;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private EncodePasswordService $encodePassword,
    ) {
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setCreatedAt'],
            BeforeEntityUpdatedEvent::class => ['setUpdatedAt'],
        ];
    }

    public function setCreatedAt(BeforeEntityPersistedEvent $event)
    {
        $entityInstance = $event->getEntityInstance();

        if (
            !$entityInstance instanceof Category
            && !$entityInstance instanceof Author
            && !$entityInstance instanceof Video
            && !$entityInstance instanceof User
        ) {
            return;
        }

        if ($entityInstance instanceof Category) {
            $entityInstance->setName(strtolower($entityInstance->getName()));
        }

        if ($entityInstance instanceof Author) {
            $entityInstance->setName(strtolower($entityInstance->getName()));
        }

        if ($entityInstance instanceof Video) {
            $entityInstance->setTitle(strtolower($entityInstance->getTitle()));
        }

        if ($entityInstance instanceof User) {
            $this->encodePassword->encode($entityInstance);
        }

        $entityInstance->setCreatedAt(new \DateTimeImmutable());
    }

    public function setUpdatedAt(BeforeEntityUpdatedEvent $event)
    {
        $entityInstance = $event->getEntityInstance();

        if (
            !$entityInstance instanceof Category
            && !$entityInstance instanceof Author
            && !$entityInstance instanceof Video
            && !$entityInstance instanceof User
        ) {
            return;
        }

        if ($entityInstance instanceof Category) {
            $entityInstance->setName(strtolower($entityInstance->getName()));
        }

        if ($entityInstance instanceof Author) {
            $entityInstance->setName(strtolower($entityInstance->getName()));
        }

        if ($entityInstance instanceof Video) {
            $entityInstance->setTitle(strtolower($entityInstance->getTitle()));
        }

        if ($entityInstance instanceof User) {
            if (!$entityInstance->getPlainPassword() && !$entityInstance->getPassword()) {
                return;
            }

            if ($entityInstance->getPlainPassword()) {
                $this->encodePassword->encode($entityInstance);
            }
        }

        $entityInstance->setUpdatedAt(new \DateTimeImmutable());
    }
}
