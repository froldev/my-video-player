# My Video Player

The only site you can save and see the YouTube, Vimeo, Dailymotion and Facebook videos with 
- [Symfony 6](https://symfony.com),
- [Docker](https://www.docker.com/),
- [Php 8.2](https://www.php.net/releases/8.2/en.php/),
- [PostgreSQL 15](https://www.postgresql.org/),
- [Bootstrap 5.2.3](https://blog.getbootstrap.com/2022/11/22/bootstrap-5-2-3//)
- [Caddy 2](https://caddyserver.com/).


![CI](https://github.com/dunglas/symfony-docker/workflows/CI/badge.svg)

## Getting Started

Clone the project

```bash
  git clone https://gitlab.com/froldev/my-video-player.git myvideoplayer
```

Create .env.local

Modify with your datas in the docker-compose.yml
- DATABASE_URL:
- POSTGRES_DB:
- POSTGRES_PASSWORD:
- POSTGRES_USER:

For a local environment, add in the .env.local file and add your datas in DATABASE8URL

```bash
APP_ENV=dev
```

For online, add to the .env.local file

```bash
APP_ENV=prod
```

Run the docker-compose

```bash
  docker compose up -d --build
```

Log into the PHP container

```bash
  docker exec -it --user root myvideoplayer_php bash
```

Install your Symfony application

```bash
  make install
  exit
```

*Your application is available at http://127.0.0.1:8080*

## Ready to use with

This docker-compose provides you :

- PHP-8.2
    - Composer
    - Symfony CLI
    - and some other php extentions
    - nodejs, npm, yarn, wget, make, ...
- postreSQL 15
- caddy 2.7.6

## Requirements

- [Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Author

Froldev
- [Website](https://fredolive.fr)
- [Gitlab](https://gitlab.com/froldev)
- [LinkedIn](https://www.linkedin.com/in/fred-olive/)
